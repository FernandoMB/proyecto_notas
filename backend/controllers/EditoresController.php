<?php

namespace backend\controllers;

use Yii;
use common\models\Editores;
use common\models\EditoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\UploadForm;

/**
 * EditoresController implements the CRUD actions for Editores model.
 */
class EditoresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Editores models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EditoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Editores model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Editores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Editores();

        if ($model->load(Yii::$app->request->post())) {
                
                $imageForm = new UploadForm();
                $imageForm->imageFile = UploadedFile::getInstance($model, 'imageFile');
                $id = Editores::find()->max('id');
                $maxId = $id+1;
                $fecha = date('d-m-Y');
                $nombre = 'foto_editor_id_'.$maxId.'_fecha_'.$fecha;
                $model->nombre_completo = $model->nombre.' '.$model->apellido;
                if($imageForm->imageFile != NULL ){
                    $model->foto = $nombre . '.' . $imageForm->imageFile->extension;
                    $imageForm->upload($nombre);
                }else{
                    $model->foto = 'No-image-found.jpg';
                }
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
                
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Editores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
                $imageForm = new UploadForm();
                $imageForm->imageFile = UploadedFile::getInstance($model, 'imageFile');
                $maxId = $model->id;
                $fecha = date('d-m-Y');
                $nombre = 'foto_editor_id_'.$maxId.'_fecha_'.$fecha;
                $model->nombre_completo = $model->nombre.' '.$model->apellido;
                if($imageForm->imageFile != NULL ){
                    unlink('../web/fotos/'.$model->foto);
                    $model->foto = $nombre . '.' . $imageForm->imageFile->extension;
                    $imageForm->upload($nombre);
                }
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                } 
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Editores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        unlink('../web/fotos/'.$model->foto);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Editores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Editores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Editores::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
