<?php

namespace backend\controllers;

use Yii;
use common\models\Notas;
use common\models\BuscadorNotas;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\UploadForm;
use common\models\Editores;
use common\models\NotaTieneEditores;
use common\models\NotaTieneCategoria;
use common\models\Categorias;

/**
 * NotasController implements the CRUD actions for Notas model.
 */
class NotasController extends Controller
{
    public $editores_colab;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BuscadorNotas();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $editores_colab = $this->getEditoresColab($id);
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            'editores_colab' => $editores_colab,
            'categoria' => $this->getCategoria($model->categoria),
        ]);
    }

    /**
     * Creates a new Notas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Notas();
        $model->fecha_creacion = date('Y-m-d');
        $model->fecha_publicacion = date('Y-m-d',  strtotime('+2 day'));
        
        if ($model->load(Yii::$app->request->post())) {
                
                $id = Notas::find()->max('id');
                $maxId = $id+1;
                $fecha = date('d-m-Y');
                $nombre = 'nota_id_'.$maxId.'_fecha_'.$fecha;
                $imageForm = new UploadForm();
                $imageForm->imageFile = UploadedFile::getInstance($model, 'imageFile');
                if($imageForm->imageFile != NULL ){
                    $model->foto = $nombre . '.' . $imageForm->imageFile->extension;
                    $imageForm->upload($nombre);
                }else{
                    $model->foto = 'No-image-found.jpg';
                }
                
                if($model->editores_colab != NULL){
                    $this->setEditoresColab($model->id, $model->editores_colab);
                }
                
                if($model->save()){
                        return $this->redirect(['view', 'id' => $model->id]);
                }
        }
        return $this->render('create', [
            'model' => $model,
            'editores' => Editores::find()->all(),
            'categorias' => Categorias::find()->all(),
        ]);
    }

    /**
     * Updates an existing Notas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->editores_colab = $this->getEditoresColab($id);

        if ($model->load(Yii::$app->request->post())) {
                
                $maxId = $model->id;
                $fecha = date('d-m-Y');
                $nombre = 'nota_id_'.$maxId.'_fecha_'.$fecha;
                $imageForm = new UploadForm();
                $imageForm->imageFile = UploadedFile::getInstance($model, 'imageFile');
                if($imageForm->imageFile != NULL ){
                    unlink('../web/fotos/'.$model->foto);
                    $model->foto = $nombre . '.' . $imageForm->imageFile->extension;
                    $imageForm->upload($nombre);
                }
                
                if($model->editores_colab != NULL){
                    NotaTieneEditores::deleteAll(['id_nota' => $id]);
                    $this->setEditoresColab($model->id, $model->editores_colab);
                }
                
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]); 
                }
        }

        return $this->render('update', [
            'model' => $model,
            'editores' => Editores::find()->all(),
            'categorias' => Categorias::find()->all(),
            'categoria' => $this->getCategoria($model->categoria),
        ]);
    }

    /**
     * Deletes an existing Notas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        unlink('../web/fotos/'.$model->foto);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Notas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    protected function setEditoresColab($id, $editores_colab){
        
        foreach($editores_colab as $editor){
            $notaTE = new NotaTieneEditores();
            $notaTE->id_nota = $id;
            $notaTE->id_editor_colaborador = Editores::findOne(['nombre_completo' => $editor])->id;
            $notaTE->save();
            unset($notaTE);
        }
    }
    
    protected function getCategoria($id){
        $categoria = Categorias::findOne(['id' => $id])->nombre;
        return $categoria;
    }
    
    public function desarreglar($doblearreglo){
        $subarray = [];
        $arreglo = [];
        foreach($doblearreglo as $subarray){
            foreach($subarray as $item){
                array_push($arreglo, $item);
            }
        }
        return $arreglo;
    }
    
    protected function getEditoresColab($id){
        $doble_arreglo_id = NotaTieneEditores::find()
                                        ->asArray()
                                        ->select(['id_editor_colaborador'])
                                        ->where(['id_nota' => $id])
                                        ->all();
        
        $arreglo_id = $this->desarreglar($doble_arreglo_id);
        
        $doble_arreglo_editores = Editores::find()
                                        ->asArray()
                                        ->select(['nombre_completo'])
                                        ->where(['id' => $arreglo_id])
                                        ->all();
        
        $sql = 'SELECT editores.nombre_completo FROM editores LEFT JOIN nota_tiene_editores ON editores.id = nota_tiene_editores.id_editor_colaborador
WHERE nota_tiene_editores.id_nota = 3';
        
        
        $arreglo_editores = $this->desarreglar($doble_arreglo_editores);
        
        if($arreglo_editores == []){
            $arreglo_editores[] = 'Ninguno';
        }
        
        return $arreglo_editores;
    }
}
