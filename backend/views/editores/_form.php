<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Editores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="editores-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>
    
    <?= Html::img('@web/fotos/'.$model->foto, ['alt' => 'foto', 'height' => 200, 'width' => 200]) ?>
    
    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <h4>Fecha de Nacimiento</h4>
    <?php
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'fechanac',
            //'dateFormat' => 'yyyy-MM-dd',
        ]);
    ?><br>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'actividad')->dropDownList([ 'activo' => 'Activo', 'inactivo' => 'Inactivo', ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
