<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Editores */

$this->title = 'Crear Editores';
$this->params['breadcrumbs'][] = ['label' => 'Editores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="editores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
