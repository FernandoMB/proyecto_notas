<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EditoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Editores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="editores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Editores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'apellido',
            'fechanac',
            'email:email',
            'actividad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
