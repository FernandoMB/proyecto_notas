<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Editores */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Editores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="editores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'apellido',
            [
                'label' => 'Foto',
                'value' => Yii ::getAlias('@web').'/fotos/'.$model->foto,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            'fechanac',
            'email:email',
            'actividad',
        ],
    ]) ?>

</div>
