<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Notas */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="notas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categoria')->dropDownList(ArrayHelper::map($categorias, 'id', 'nombre')) ?>
            
    <?= $form->field($model, 'epigrafe')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'nota')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'editor_creador')->dropDownList(ArrayHelper::map($editores, 'nombre_completo', 'nombre_completo')) ?>
    
    <?= $form->field($model, 'editores_colab')->checkboxList(ArrayHelper::map($editores, 'nombre_completo', 'nombre_completo'))?>

    <h4>Fecha de Creacion</h4>
    <?php
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'fecha_creacion',   
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'defaultDate' => date('Y-m-d'),
                'todayHighlight' => true
            ]
        ]);
    ?>
    <h4>Fecha de Publicacion</h4>
    <?php
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'fecha_publicacion',
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'defaultDate' => date('Y-m-d'),
            ]
        ]);
    ?>
    <br>
    <?= $form->field($model, 'estado_publicacion')->dropDownList([ 'no' => 'No', 'si' => 'Si']) ?>

    <?= Html::img('@web/fotos/'.$model->foto, ['alt' => 'foto', 'height' => 200, 'width' => 200]) ?>
            
    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end();?>

</div>
