<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\BuscadorNotas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'titulo') ?>

    <?= $form->field($model, 'epigrafe') ?>

    <?= $form->field($model, 'editor_creador') ?>
    <h4>Fecha Inicial de Creacion</h4>
    <?php
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'fecha_inicial',
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]);
    ?>
    <br>
    <h4>Fecha Final de Creacion</h4>
    <?php
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'fecha_final',
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]);
    ?>
    <br>

    <?php // echo $form->field($model, 'estado_publicacion') ?>

    <?php // echo $form->field($model, 'foto') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
