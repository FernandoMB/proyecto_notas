<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BuscadorNotas */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Notas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'titulo:ntext',
            'epigrafe:ntext',
            'nota:ntext',
            'editor_creador',
            'fecha_creacion',
            //'fecha_publicacion',
            'estado_publicacion',
            //'foto',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'Acciones',
                'template' => '{view} {update} {delete}',
                'visibleButtons'=>[
                    'delete'=> function($model){
                                return $model->estado_publicacion!='si';
                            },       
                ],
            ],
        ]
    ]); ?>
    
    <h2>Buscar notas</h2>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>


</div>
