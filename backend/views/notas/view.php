<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Notas */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Notas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="notas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if($model->estado_publicacion=='no'){ 
                    echo Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                        ],
                    ]) ;
                            
        } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'titulo',
            [
                'label' => 'Categoria',
                'value' => $categoria,
            ],
            'epigrafe:ntext',
            'nota:ntext',
            'editor_creador',
            [
                'label' => 'Editores Colaboradores',
                'value' => implode(', ', $editores_colab),
            ],
            'fecha_creacion',
            'fecha_publicacion',
            'estado_publicacion',
            [
                'label' => 'Foto',
                'value' => Yii ::getAlias('@web').'/fotos/'.$model->foto,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
        ],
    ]) ?>
    

</div>
