<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Indice</h1>
    </div>

    <div class="body-content">

       <h2>Editores</h2>
    
        <p><a class="btn btn-default" href="/proyecto_notas/backend/web/index.php/editores/index">Ir &raquo;</a></p>
    
        <h2>Notas</h2>
    
        <p><a class="btn btn-default" href="/proyecto_notas/backend/web/index.php/notas/index">Ir &raquo;</a></p>
        
        <h2>Administracion</h2>
        
        <p>
            
            <a class="btn btn-default" href="/proyecto_notas/backend/web/admin/user">Usuarios &raquo;</a>
            
            <a class="btn btn-default" href="/proyecto_notas/backend/web/admin/permission">Permisos &raquo;</a>
            
            <a class="btn btn-default" href="/proyecto_notas/backend/web/admin/role">Roles &raquo;</a>
            
            <a class="btn btn-default" href="/proyecto_notas/backend/web/admin/route">Rutas &raquo;</a>
            
            <a class="btn btn-default" href="/proyecto_notas/backend/web/admin/assignment">Asignaciones &raquo;</a>
            
            <a class="btn btn-default" href="/proyecto_notas/backend/web/admin/menu">Menu &raquo;</a>
            
        </p>

    </div>
    
</div>
