<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Notas;

/**
 * BuscadorNotas represents the model behind the search form of `common\models\Notas`.
 */
class BuscadorNotas extends Notas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'fecha_publicacion'], 'integer'],
            [['titulo', 'epigrafe', 'nota', 'editor_creador', 
               'fecha_creacion', 'estado_publicacion', 'foto', 
               'fecha_inicial', 'fecha_final'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_publicacion' => $this->fecha_publicacion,
        ]);

        $query->andFilterWhere(['like', 'titulo', $this->titulo])
            ->andFilterWhere(['like', 'epigrafe', $this->epigrafe])
            ->andFilterWhere(['like', 'nota', $this->nota])
            ->andFilterWhere(['like', 'editor_creador', $this->editor_creador])
            ->andFilterWhere(['like', 'estado_publicacion', $this->estado_publicacion])
            ->andFilterWhere(['like', 'foto', $this->foto]);
        
        $query->andFilterWhere(['between', 'fecha_creacion', $this->fecha_inicial, $this->fecha_final]);

        return $dataProvider;
    }
}
