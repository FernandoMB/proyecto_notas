<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * This is the model class for table "editores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellido
 * @property string $foto
 * @property string $fechanac
 * @property string $email
 * @property string $actividad
 */
class Editores extends \yii\db\ActiveRecord
{
    public $imageFile;
    
    public static function tableName()
    {
        return 'editores';
    }

        public function rules()
    {
        return [
            [['nombre', 'apellido', 'fechanac', 'email'], 'required'],
            [['foto', 'actividad'], 'string'],
            [['fechanac'], 'safe'],
            [['nombre', 'apellido'], 'string', 'max' => 50],
            [['email', 'nombre_completo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'foto' => 'Foto',
            'fechanac' => 'Fecha Nacimiento',
            'email' => 'Email',
            'actividad' => 'Actividad',
            'imageFile' => 'Foto',
            'nombre_completo' => 'Nombre Completo'
        ];
    }
    
}
