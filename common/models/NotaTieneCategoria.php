<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "nota_tiene_categoria".
 *
 * @property int $id
 * @property int $id_nota
 * @property int $id_categoria
 */
class NotaTieneCategoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nota_tiene_categoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_nota', 'id_categoria'], 'required'],
            [['id_nota', 'id_categoria'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_nota' => 'Id Nota',
            'id_categoria' => 'Id Categoria',
        ];
    }
}
