<?php

namespace common\models;

use Yii;
use common\Editores;

/**
 * This is the model class for table "nota_tiene_editores".
 *
 * @property int $id
 * @property int $id_nota
 * @property int $id_editor_colaborador
 */
class NotaTieneEditores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nota_tiene_editores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_nota', 'id_editor_colaborador'], 'required'],
            [['id_nota', 'id_editor_colaborador'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_nota' => 'Id Nota',
            'id_editor_colaborador' => 'Id Editor Colaborador',
        ];
    }
}
