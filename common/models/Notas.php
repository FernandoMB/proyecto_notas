<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notas".
 *
 * @property int $id
 * @property string $titulo
 * @property string $epigrafe
 * @property string $nota
 * @property string $editor_creador
 * @property string $fecha_creacion
 * @property int $fecha_publicacion
 * @property string $estado_publicacion
 * @property string $foto
 */
class Notas extends \yii\db\ActiveRecord
{
    public $imageFile;
    public $fecha_inicial;
    public $fecha_final;
    public $editores_colab; 
    
    
    public static function tableName()
    {
        return 'notas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'epigrafe', 'nota', 'editor_creador', 'fecha_creacion', 'fecha_publicacion', 'foto'], 'required'],
            [['titulo', 'epigrafe', 'nota', 'estado_publicacion', 'fecha_publicacion'], 'string'],
            [['fecha_creacion', 'fecha_publicacion', 'editores_colab', 'categoria'], 'safe'],
            [['editor_creador'], 'string', 'max' => 30],
            [['foto'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'epigrafe' => 'Epigrafe',
            'nota' => 'Nota',
            'editor_creador' => 'Editor Creador',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_publicacion' => 'Fecha Publicacion',
            'estado_publicacion' => 'Estado Publicacion',
            'foto' => 'Foto',
            'imageFile' => 'Foto',
            'fecha_inicial' => 'Fecha Inicial de Creacion',
            'fecha_final' => 'Fecha Final de Creacion',
            'editores_colab' => 'Editores Colaboradores',
            'categoria' => 'Categoria'
        ];
    }
}
