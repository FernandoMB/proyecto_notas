<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%notas}}`.
 */
class m191028_131938_create_notas_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notas', [
            'id' => $this->primaryKey(),
            'titulo' => $this->text(),
            'epigrafe' => $this->text(),
            'nota' => $this->text(),
            'editor_creador' => $this->string(30)->notNull(),
            'fecha_creacion' => $this->date()->notNull(),
            'fecha_publicacion' => $this->date()->notNull(),
            'estado_publicacion' => $this->string(20)->notNull(),
            'foto' => $this->string(100)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%notas}}');
    }
}
