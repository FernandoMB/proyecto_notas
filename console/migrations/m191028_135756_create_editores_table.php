<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%editores}}`.
 */
class m191028_135756_create_editores_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('%editores', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
            'apellido' => $this->string(50)->notNull(),
            'nombre_completo' =>$this->string(100)->notNull(),
            'foto' => $this->text()->notNull(),
            'fechanac' => $this->date(),
            'email' => $this->string(50)->notNull(),
            'actividad' => $this->string(20)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%editores}}');
    }
}
