<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%nota_tiene_editores}}`.
 */
class m191028_142013_create_nota_tiene_editores_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('nota_tiene_editores', [
            'id' => $this->primaryKey(),
            'id_nota' => $this->integer(20)->notNull(),
            'id_editor_colaborador' => $this->integer(20)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%nota_tiene_editores}}');
    }
}
