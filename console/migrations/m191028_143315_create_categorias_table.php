<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%categorias}}`.
 */
class m191028_143315_create_categorias_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('categorias', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(20)
        ]);
        $this->batchInsert('categorias', ['nombre'], [
                                                ['Deportes'], 
                                                ['Politica'], 
                                                ['Entretenimiento'], 
                                                ['Infantil'],
                            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%categorias}}');
    }
}
